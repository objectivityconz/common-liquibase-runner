package nz.co.objectivity.war.liquibase;


import liquibase.exception.CommandLineParsingException;
import liquibase.integration.commandline.MainTrunker;

import java.io.File;
import java.io.IOException;

public class LiquibaseRunner {

  public static void run(File file, String[] args) {
    new LiquibaseRunner(file, args);
  }

  public LiquibaseRunner(File file, String[] args) {
    try {
      if (args.length == 0)
        MainTrunker.main(new String[]{"--help"});
      else
        MainTrunker.main(args);
      System.exit(0);
    } catch (CommandLineParsingException e) {
      e.printStackTrace();
      System.exit(-1);
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(-2);
    }
  }
}
