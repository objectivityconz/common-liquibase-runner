package liquibase.servicelocator;

import com.bluetrainsoftware.classpathscanner.ClasspathScanner;
import com.bluetrainsoftware.classpathscanner.ResourceScanListener;

import java.io.InputStream;
import java.util.*;

/**
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
public class FastPackageScanClassResolver implements PackageScanClassResolver {
	private Set<ClassLoader> classLoaders = new HashSet<ClassLoader>();
	private Set<PackageScanFilter> scanFilters;

	@Override
	public void setClassLoaders(Set<ClassLoader> classLoaders) {
		this.classLoaders.clear();
		this.classLoaders.addAll(classLoaders);
	}

	@Override
	public Set<ClassLoader> getClassLoaders() {
		return classLoaders;
	}

	@Override
	public void addClassLoader(ClassLoader classLoader) {
		this.classLoaders.add(classLoader);
	}

	@Override
	public Set<Class<?>> findImplementations(final Class<?> parent, final String... packageNames) {
		PackageScanFilter composedFilter = getCompositeFilter(new AssignableToPackageScanFilter(parent));

		return findByFilter(composedFilter, packageNames);
	}

	private PackageScanFilter getCompositeFilter(PackageScanFilter filter) {
		if (scanFilters != null) {
			CompositePackageScanFilter composite = new CompositePackageScanFilter(scanFilters);
			composite.addFilter(filter);
			return composite;
		}
		return filter;
	}

	@Override
	public Set<Class<?>> findByFilter(final PackageScanFilter filter, String... packageNames) {
		ClasspathScanner scanner = new ClasspathScanner();

		final ArrayList<String> packagePrefixes = new ArrayList<String>();
		final HashSet<Class<?>> results = new HashSet<Class<?>>();

		for (String pkg : packageNames) {
			packagePrefixes.add(pkg.replace('.', '/'));
		}

		scanner.registerResourceScanner(new ResourceScanListener() {
			@Override
			public List<ScanResource> resource(List<ScanResource> scanResources) throws Exception {
				ArrayList<ResourceScanListener.ScanResource> resources = new ArrayList<ResourceScanListener.ScanResource>();

				for (ScanResource scanResource : scanResources) {
					if (scanResource.resourceName.endsWith(".class")) {
						for (String pkgPrefix : packagePrefixes) {
							if (scanResource.resourceName.startsWith(pkgPrefix)) {
								resources.add(scanResource);
								break;
							}
						}
					}
				}

				return resources;
			}

			@Override
			public void deliver(ScanResource desire, InputStream inputStream) {
				try {
					Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass(desire.resourceName.substring(0, desire.resourceName.length() - 6).replace('/', '.'));
//					Class<?> clazz = Class.forName(desire.resourceName.substring(0, desire.resourceName.length() - 6).replace('/', '.'));

					if (filter != null && filter.matches(clazz)) {
						results.add(clazz);
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}

			@Override
			public InterestAction isInteresting(InterestingResource interestingResource) {
				return InterestAction.ONCE; // only tell me once, this scanner will disappear thereafter
			}

			@Override
			public void scanAction(ScanAction action) {
			}
		});


		for (ClassLoader cl : classLoaders) {
			scanner.scan(cl, true);
		}

//		resources.clear();

		return new HashSet<Class<?>>(results);
	}


	@Override
	public void addFilter(PackageScanFilter filter) {
		if (scanFilters == null) {
			scanFilters = new LinkedHashSet<PackageScanFilter>();
		}
		scanFilters.add(filter);
	}

	@Override
	public void removeFilter(PackageScanFilter filter) {
		if (scanFilters != null) {
			scanFilters.remove(filter);
		}
	}
}
