package liquibase.servicelocator;

import liquibase.resource.ClassLoaderResourceAccessor;

/**
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
public class FastServiceLocator extends ServiceLocator {
	public FastServiceLocator() {
		super(new FastPackageScanClassResolver(), new ClassLoaderResourceAccessor(Thread.currentThread().getContextClassLoader()));
	}
}
