package liquibase.integration.commandline;

import liquibase.Liquibase;
import liquibase.exception.CommandLineParsingException;
import liquibase.exception.ValidationFailedException;
import liquibase.logging.LogFactory;
import liquibase.logging.LogLevel;
import liquibase.logging.Logger;
import liquibase.servicelocator.FastServiceLocator;
import liquibase.servicelocator.ServiceLocator;
import liquibase.util.LiquibaseUtil;
import liquibase.util.StreamUtil;
import liquibase.util.StringUtils;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MainTrunker extends Main {

	public static void main(String args[]) throws CommandLineParsingException, IOException {


		try {
			String shouldRunProperty = System.getProperty(Liquibase.SHOULD_RUN_SYSTEM_PROPERTY);
			if (shouldRunProperty != null && !Boolean.valueOf(shouldRunProperty)) {
				System.err.println("Liquibase did not run because '" + Liquibase.SHOULD_RUN_SYSTEM_PROPERTY + "' system property was set to false");
				return;
			}

			MainTrunker main = new MainTrunker();
			main.configureClassLoader();
			if (args.length == 1 && "--help".equals(args[0])) {
				main.printHelp(System.err);
				return;
			} else if (args.length == 1 && "--version".equals(args[0])) {
				System.err.println("Liquibase Version: " + LiquibaseUtil.getBuildVersion() + StreamUtil.getLineSeparator());
				return;
			}

			try {
				main.parseOptions(args);
			} catch (CommandLineParsingException e) {
				main.printHelp(Arrays.asList(e.getMessage()), System.err);
				System.exit(-2);
			}

			File propertiesFile = new File(main.defaultsFile);
			File localPropertiesFile = new File(main.defaultsFile.replaceFirst("(\\.[^\\.]+)$", ".local$1"));

			if (localPropertiesFile.exists()) {
				main.parsePropertiesFile(new FileInputStream(localPropertiesFile));
			}
			if (propertiesFile.exists()) {
				main.parsePropertiesFile(new FileInputStream(propertiesFile));
			}

			List<String> setupMessages = main.checkSetup();
			if (setupMessages.size() > 0) {
				main.printHelp(setupMessages, System.err);
				return;
			}

			try {
				main.applyDefaults();
				main.doMigration();
			} catch (Throwable e) {
				String message = e.getMessage();
				if (message == null) {
					message = "Unknown Reason";
				}

				if (e.getCause() instanceof ValidationFailedException) {
					((ValidationFailedException) e.getCause()).printDescriptiveError(System.err);
				} else {
					System.err.println("Liquibase " + main.command + " Failed: " + message);
					LogFactory.getLogger().severe(message, e);
					System.err.println(generateLogLevelWarningMessage());
					e.printStackTrace();
				}
				System.exit(-1);
			}

			if ("update".equals(main.command)) {
				System.err.println("Liquibase Update Successful");
			} else if (main.command.startsWith("rollback") && !main.command.endsWith("SQL")) {
				System.err.println("Liquibase Rollback Successful");
			} else if (!main.command.endsWith("SQL")) {
				System.err.println("Liquibase '" + main.command + "' Successful");
			}
		} catch (Throwable e) {
			String message = "Unexpected error running Liquibase: " + e.getMessage();
			System.err.println(message);
			try {
				LogFactory.getLogger().severe(message, e);
			} catch (Exception e1) {
				e.printStackTrace();
			}
			System.exit(-3);
		}
		System.exit(0);
	}

	private static String generateLogLevelWarningMessage() {
		Logger logger = LogFactory.getLogger();
		if (logger != null && logger.getLogLevel() != null && (logger.getLogLevel().equals(LogLevel.OFF))) {
			return "";
		} else {
			return "\n\nFor more information, use the --logLevel flag";
		}
	}

	@Override
	protected void configureClassLoader() throws CommandLineParsingException {
		ServiceLocator.setInstance(new FastServiceLocator());
		classLoader = Thread.currentThread().getContextClassLoader();
	}


	private String[] splitArg(String arg) throws CommandLineParsingException {
		String[] splitArg = arg.split("=", 2);
		if (splitArg.length < 2) {
			throw new CommandLineParsingException("Could not parse '" + arg + "'");
		}

		splitArg[0] = splitArg[0].replaceFirst("--", "");
		return splitArg;
	}

	private void checkForMalformedCommandParameters(final List<String> messages) {
		if (!commandParams.isEmpty()) {
			if ("calculateCheckSum".equalsIgnoreCase(command)) {
				for (final String param : commandParams) {
					assert param != null;
					if (param != null && !param.startsWith("-")) {
						final String[] parts = param.split("::");
						if (parts == null || parts.length < 3) {
							messages.add("changeSet identifier must be of the form filepath::id::author");
							break;
						}
					}
				}
			}
		}
	}

	private boolean isChangeLogRequired(String command) {
		return command.toLowerCase().startsWith("update")
				|| command.toLowerCase().startsWith("rollback")
				|| "calculateCheckSum".equalsIgnoreCase(command)
				|| "status".equalsIgnoreCase(command)
				|| "validate".equalsIgnoreCase(command)
				|| "changeLogSync".equalsIgnoreCase(command)
				|| "changeLogSyncSql".equalsIgnoreCase(command);
	}

	private boolean isCommand(String arg) {
		return "migrate".equals(arg)
				|| "migrateSQL".equalsIgnoreCase(arg)
				|| "update".equalsIgnoreCase(arg)
				|| "updateSQL".equalsIgnoreCase(arg)
				|| "updateCount".equalsIgnoreCase(arg)
				|| "updateCountSQL".equalsIgnoreCase(arg)
				|| "rollback".equalsIgnoreCase(arg)
				|| "rollbackToDate".equalsIgnoreCase(arg)
				|| "rollbackCount".equalsIgnoreCase(arg)
				|| "rollbackSQL".equalsIgnoreCase(arg)
				|| "rollbackToDateSQL".equalsIgnoreCase(arg)
				|| "rollbackCountSQL".equalsIgnoreCase(arg)
				|| "futureRollbackSQL".equalsIgnoreCase(arg)
				|| "futureRollbackCountSQL".equalsIgnoreCase(arg)
				|| "updateTestingRollback".equalsIgnoreCase(arg)
				|| "tag".equalsIgnoreCase(arg)
				|| "listLocks".equalsIgnoreCase(arg)
				|| "dropAll".equalsIgnoreCase(arg)
				|| "releaseLocks".equalsIgnoreCase(arg)
				|| "status".equalsIgnoreCase(arg)
				|| "unexpectedChangeSets".equalsIgnoreCase(arg)
				|| "validate".equalsIgnoreCase(arg)
				|| "help".equalsIgnoreCase(arg)
				|| "diff".equalsIgnoreCase(arg)
				|| "diffChangeLog".equalsIgnoreCase(arg)
				|| "generateChangeLog".equalsIgnoreCase(arg)
				|| "calculateCheckSum".equalsIgnoreCase(arg)
				|| "clearCheckSums".equalsIgnoreCase(arg)
				|| "dbDoc".equalsIgnoreCase(arg)
				|| "changelogSync".equalsIgnoreCase(arg)
				|| "changelogSyncSQL".equalsIgnoreCase(arg)
				|| "markNextChangeSetRan".equalsIgnoreCase(arg)
				|| "markNextChangeSetRanSQL".equalsIgnoreCase(arg);
	}

	private boolean isNoArgCommand(String arg) {
		return "migrate".equals(arg)
				|| "migrateSQL".equalsIgnoreCase(arg)
				|| "update".equalsIgnoreCase(arg)
				|| "updateSQL".equalsIgnoreCase(arg)
				|| "futureRollbackSQL".equalsIgnoreCase(arg)
				|| "updateTestingRollback".equalsIgnoreCase(arg)
				|| "listLocks".equalsIgnoreCase(arg)
				|| "dropAll".equalsIgnoreCase(arg)
				|| "releaseLocks".equalsIgnoreCase(arg)
				|| "validate".equalsIgnoreCase(arg)
				|| "help".equalsIgnoreCase(arg)
				|| "clearCheckSums".equalsIgnoreCase(arg)
				|| "changelogSync".equalsIgnoreCase(arg)
				|| "changelogSyncSQL".equalsIgnoreCase(arg)
				|| "markNextChangeSetRan".equalsIgnoreCase(arg)
				|| "markNextChangeSetRanSQL".equalsIgnoreCase(arg);
	}


	protected void parseOptions(String[] args) throws CommandLineParsingException {
		args = fixupArgs(args);

		boolean seenCommand = false;
		for (String arg : args) {
			if (isCommand(arg)) {
				this.command = arg;
				if (this.command.equalsIgnoreCase("migrate")) {
					this.command = "update";
				} else if (this.command.equalsIgnoreCase("migrateSQL")) {
					this.command = "updateSQL";
				}
				seenCommand = true;
			} else if (seenCommand) {
				if (arg.startsWith("-D")) {
					String[] splitArg = splitArg(arg);

					String attributeName = splitArg[0].replaceFirst("^-D", "");
					String value = splitArg[1];

					changeLogParameters.put(attributeName, value);
				} else {
					commandParams.add(arg);
				}
			} else if (arg.startsWith("--")) {
				String[] splitArg = splitArg(arg);

				String attributeName = splitArg[0];
				String value = splitArg[1];

				if (StringUtils.trimToEmpty(value).equalsIgnoreCase("PROMPT")) {
					Console c = System.console();
					if (c == null) {
						throw new CommandLineParsingException("Console unavailable");
					}
					//Prompt for value
					if (attributeName.toLowerCase().contains("password")) {
						value = new String(c.readPassword(attributeName + ": "));
					} else {
						value = new String(c.readLine(attributeName + ": "));
					}
				}

				try {
					Field field = getClass().getSuperclass().getDeclaredField(attributeName);
					if (field.getType().equals(Boolean.class)) {
						field.set(this, Boolean.valueOf(value));
					} else {
						field.set(this, value);
					}
				} catch (Exception e) {
					throw new CommandLineParsingException("Unknown parameter: '" + attributeName + "'");
				}
			} else {
				throw new CommandLineParsingException("Unexpected value " + arg + ": parameters must start with a '--'");
			}
		}

	}

	protected void parsePropertiesFile(InputStream propertiesInputStream) throws IOException, CommandLineParsingException {
		Properties props = new Properties();
		props.load(propertiesInputStream);

		for (Map.Entry entry : props.entrySet()) {
			try {
				if (entry.getKey().equals("promptOnNonLocalDatabase")) {
					continue;
				}
				if (((String) entry.getKey()).startsWith("parameter.")) {
					changeLogParameters.put(((String) entry.getKey()).replaceFirst("^parameter.", ""), entry.getValue());
				} else {
					Field field = getClass().getSuperclass().getDeclaredField((String) entry.getKey());
					Object currentValue = field.get(this);

					if (currentValue == null) {
						String value = entry.getValue().toString().trim();
						if (field.getType().equals(Boolean.class)) {
							field.set(this, Boolean.valueOf(value));
						} else {
							field.set(this, value);
						}
					}
				}
			} catch (Exception e) {
				throw new CommandLineParsingException("Unknown parameter: '" + entry.getKey() + "'");
			}
		}
	}


}
